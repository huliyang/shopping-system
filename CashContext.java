package jdf;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wuliyang
 * @date 2024/4/28 16:46
 * @description
 */
public class CashContext {

    private CashSuper cashSuper;

    private ShoppingCart shoppingCart;

    public CashContext(int cashType,ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
        switch (cashType) {
            case 1:
                this.cashSuper = new CashNormal();
                break;
            case 2:
                Map<Fruit,Double> discounts =  new HashMap<>();
                discounts.put(Fruit.STRAWBERRY,0.8d);
                this.cashSuper = new CashRebate(discounts);
                break;
            case 3:
                this.cashSuper = new CashReturn(300d,100d);
                break;
            default:
                this.cashSuper = new CashNormal();
        }
    }

    public double getResult(){
        return cashSuper.acceptCash(shoppingCart);
    };
}
