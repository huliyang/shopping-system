package jdf;

/**
 * @author wuliyang
 * @date 2024/4/28 19:12
 * @description
 */
public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        System.out.println("one ============");
        main.one();
        System.out.println("two ============");
        main.two();
        System.out.println("three ============");
        main.three();
        System.out.println("four ============");
        main.four();
    }
    /**
     *  1、有一家超市，出售苹果和草莓。其中苹果 8 元/斤，草莓 13 元/斤。
     * 现在顾客 A 在超市购买了若干斤苹果和草莓，需要计算一共多少钱？
     * 请编写函数，对于 A 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价。
     */
    public void one(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.loadCar(Fruit.APPLE,10);
        shoppingCart.loadCar(Fruit.STRAWBERRY,5);
        CashContext cashContext = new CashContext(1,shoppingCart);
        double result = cashContext.getResult();
        System.out.println(result);
        System.out.println(10*8 + 13*5);
    }
    /**
     *2、超市增加了一种水果芒果，其定价为 20 元/斤。
     * 现在顾客 B 在超市购买了若干斤苹果、 草莓和芒果，需计算一共需要多少钱？
     * 请编写函数，对于 B 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价。
     */
    public void two(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.loadCar(Fruit.APPLE,5);
        shoppingCart.loadCar(Fruit.STRAWBERRY,5);
        shoppingCart.loadCar(Fruit.MANGO,10);
        CashContext cashContext = new CashContext(1,shoppingCart);
        double result = cashContext.getResult();
        System.out.println(result);
        System.out.println(5*8 + 13*5 + 10*20);
    }
    /**
     *3、超市做促销活动，草莓限时打 8 折。
     * 现在顾客 C 在超市购买了若干斤苹果、 草莓和芒果，需计算一共需要多少钱？
     * 请编写函数，对于 C 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价。
     */
    public void three(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.loadCar(Fruit.APPLE,5);
        shoppingCart.loadCar(Fruit.STRAWBERRY,5);
        shoppingCart.loadCar(Fruit.MANGO,10);
        CashContext cashContext = new CashContext(2,shoppingCart);
        double result = cashContext.getResult();
        System.out.println(result);
        System.out.println(5*8 + 13*5*0.8 + 10*20);
    }
    /**
     *4、促销活动效果明显，超市继续加大促销力度，购物满 100 减 10 块。
     * 现在顾客 D 在超市购买了若干斤苹果、 草莓和芒果，需计算一共需要多少钱？
     * 请编写函数，对于 D 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价
     */
    public void four(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.loadCar(Fruit.APPLE,5);
        shoppingCart.loadCar(Fruit.STRAWBERRY,5);
        shoppingCart.loadCar(Fruit.MANGO,10);
        CashContext cashContext = new CashContext(3,shoppingCart);
        double result = cashContext.getResult();
        System.out.println(result);
        System.out.println(5*8 + 13*5 + 10*20 - 100);
    }
}
