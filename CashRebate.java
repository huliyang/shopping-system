package jdf;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author wuliyang
 * @date 2024/4/28 17:06
 * @description 打折消费
 */
public class CashRebate implements CashSuper{
    Map<Fruit,Double> discounts;
    public CashRebate(Map<Fruit,Double> discounts) {
       this.discounts = discounts;
    }

    @Override
    public double acceptCash(ShoppingCart shoppingCart) {
        HashMap<Fruit,Integer> fruits = shoppingCart.getFruits();
        Set<Fruit> set = fruits.keySet();
        double total = 0.0;
        double discount;
        for (Fruit fruit : set) {
            discount = discounts.get(fruit) == null?1.0:discounts.get(fruit);
            Double aDouble = Double.valueOf(fruits.get(fruit)*fruit.number*discount);
            total += aDouble;
        }
        return total;
    }
}
