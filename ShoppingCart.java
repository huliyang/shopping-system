package jdf;

import java.util.HashMap;

public class ShoppingCart {
    // 对应的商品枚举跟对应的数量
    private HashMap<Fruit,Integer> fruits = new HashMap<>();

    //放入产品的相关方法
    public void loadCar(Fruit fruit,int num){
        if (num<0) {
            throw new RuntimeException("商品数目不允许为0");
        }
        //为了加快结账速度，为0就不进行put，加快结账速度
        if (num>0){
            fruits.put(fruit,num);
        }
    }

    public HashMap getFruits(){
        return fruits;
    }
}
