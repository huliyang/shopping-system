package jdf;

import java.util.HashMap;
import java.util.Set;

/**
 * @author wuliyang
 * @date 2024/4/28 17:06
 * @description 正常消费
 */
public class CashNormal implements CashSuper{
    @Override
    public double acceptCash(ShoppingCart shoppingCart) {
        HashMap<Fruit,Integer> fruits = shoppingCart.getFruits();
        Set<Fruit> set = fruits.keySet();
        double total = 0.0;
        for (Fruit fruit : set) {
            Double aDouble = Double.valueOf(fruits.get(fruit)*fruit.number);
            total += aDouble;
        }
        return total;
    }
}
