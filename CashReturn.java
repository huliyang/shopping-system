package jdf;

import java.util.HashMap;
import java.util.Set;

/**
 * @author wuliyang
 * @date 2024/4/28 17:06
 * @description 返利消费
 */
public class CashReturn implements CashSuper{
    //如满300-100，则条件是300，返利值为100
    //返利条件
    final private double moneyCondition;
    //返利值
    final private double moneyReturn;

    public CashReturn(double moneyCondition, double moneyReturn) {
        if (moneyCondition<0||moneyReturn<0){
            throw new RuntimeException("不存在返利条件或者返利值为负数");
        }
        this.moneyCondition = moneyCondition;
        this.moneyReturn = moneyReturn;
    }

    @Override
    public double acceptCash(ShoppingCart shoppingCart) {
        HashMap<Fruit,Integer> fruits = shoppingCart.getFruits();
        Set<Fruit> set = fruits.keySet();
        double total = 0.0;
        for (Fruit fruit : set) {
            Double aDouble = Double.valueOf(fruits.get(fruit)*fruit.number);
            total += aDouble;
        }
        if (total>=moneyCondition){
            total -= moneyReturn;
        }
        return total;
    }
}
