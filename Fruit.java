package jdf;

/**
 * @author wuliyang
 * @date 2024/4/28 16:32
 * @description 定义水果相关枚举
 */
public enum Fruit {
    //苹果
    APPLE(8.0F),
    //草莓
    STRAWBERRY(13.0F),
    //芒果
    MANGO(20.0F);

    float number;

    Fruit(float number) {
        this.number = number;
    }


}
