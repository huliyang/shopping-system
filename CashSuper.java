package jdf;

/**
 * @author wuliyang
 * @date 2024/4/28 16:46
 * @description
 */
public interface CashSuper {
    public double acceptCash(ShoppingCart shoppingCart);

}
